import React from 'react';
import SmallCube from '../SmallCube/SmallCube'

const BigCube = (props, index) => {
    return <div className="BigCube">
        <div className="inBigCube">
            {props.squares.map(elem => (
                <SmallCube key = {elem.id}
                   id = {elem.id}
                   isOpen = {elem.isOpen}
                   hasRing = {elem.hasRing}
                   squares = {props.squares}
                    changeState = {props.changeState}
                />
            ))}
        </div>
    </div>
};

export default BigCube;