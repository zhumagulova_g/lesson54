import React from 'react';

const SmallCube = (props, index)=> {

    let smallSquareColor = "dark";
    let ring = "";

        if (props.isOpen === true) {
            smallSquareColor = "white";
            if (props.hasRing === false) {
                ring = "$";
            }else {
                ring = "";
            }
        } else {
            smallSquareColor = "dark"
        }

    return (
        <div className={smallSquareColor}
             onClick={props.changeState}
             id = {props.id}>{ring}
        </div>
    );
};

export default SmallCube;