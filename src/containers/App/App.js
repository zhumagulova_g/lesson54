import React, {useState} from 'react';
import './App.css';
import BigCube from '../../components/BigCube/BigCube';
import {nanoid} from 'nanoid';

const App = () => {
  const [squares, setSquares] = useState ([]);

  const startGame = () => {
      const random = Math.floor(Math.random()*36);
      const squaresCopy = [...squares];
      let square;
      for (let i = 0; i < 36; i++) {
          if (i === random) {
              square = {
                  isOpen: false,
                  hasRing: true,
                  id: nanoid()
              };
              squaresCopy[i] = square;
          } else {
              square = {
                  isOpen: false,
                  hasRing: false,
                  id: nanoid()
              };
              squaresCopy[i] = square;
          }
      }
      setSquares(squaresCopy);
  };

  const changeState = (id) => {
      const index = squares.findIndex(elem => elem.id === id);
        const squaresCopy = [...squares];
        const elemCopy = {...squaresCopy[index]};
        elemCopy.isOpen = true;
        squaresCopy[index] = elemCopy;
        setSquares(squaresCopy);
  };
  return (
    <div className="App">
        <div>
            <BigCube squares = {squares}
                onchangeStates = {changeState}/>
            <div><button className="reset"
                 onClick={startGame}

            >
                Start game</button></div>
        </div>
    </div>
  );
};

export default App;
